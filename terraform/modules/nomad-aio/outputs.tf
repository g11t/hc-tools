output "nomad-external-ip" {
  value = "${aws_instance.main.public_ip}"
}

variable "vpc_id" {}
variable "subnet_id" {}
variable "inbound_from" {}
variable "region" {}
variable "key_name" {}
variable "nomad_version" {
  default = "0.7.0"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "extra_sgs" {
  type    = "list"
  default = []
}

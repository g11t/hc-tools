resource "aws_iam_policy" "consul-autojoin" {
  name        = "consul-nomad-autojoin"
  path        = "/"
  description = "This policy allows a consul agent to find other consul nodes"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
    EOF
}

resource "aws_iam_role" "nomad-role" {
  name = "nomad-role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "consul-autojoin" {
  name       = "consul-discovery"
  roles      = ["${aws_iam_role.nomad-role.name}"]
  policy_arn = "${aws_iam_policy.consul-autojoin.arn}"
}

resource "aws_iam_instance_profile" "main" {
  name = "nomad-instance-profile"
  role = "${aws_iam_role.nomad-role.name}"
}

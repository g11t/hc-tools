#!/bin/bash

set -x
exec > /var/log/user-data.log 2>&1

REGION="${region}"
NOMAD_VERSION="${nomad_version}"

IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

docker run -d --net=host \
    --restart=always \
    --name=consul \
    consul agent \
    -bind="$${IP}" \
    -client=0.0.0.0 \
    -retry-join "provider=aws tag_key=Purpose tag_value=Consul-Node region=$${REGION}" \

# Set-up consul-template config
mkdir -p /tmp/haproxy-consul-template/{config.d,templates}
cat <<EOF > /tmp/haproxy-consul-template/config.d/consul-template.conf
consul = "172.17.0.1:8500"

template {
  source = "/consul-template/templates/haproxy.ctmpl"
  destination = "/etc/haproxy/haproxy.cfg"
  command = "restart-haproxy"
  perms = 0600
  backup = true
  left_delimiter  = "{{"
  right_delimiter = "}}"
  wait = "2s:6s"
}
EOF

# Set-up the consul-template for HAproxy
cat <<EOF > /tmp/haproxy-consul-template/templates/haproxy.ctmpl
global
    maxconn {{key_or_default "service/www-demo-backend-nginx/maxconn" "5000"}}
    chroot /var/lib/haproxy
    user haproxy
    group haproxy
    daemon

defaults
    mode {{key_or_default "service/www-demo-backend-nginx/mode" "tcp"}}
    timeout connect 5000
    timeout client 50000
    timeout server 50000

listen tcp-in
    mode tcp
    balance roundrobin
    bind *:80
    {{range \$tag, \$services := services | byTag}}{{if eq \$tag "routed"}}{{range \$service := \$services}}{{range service \$service.Name}}server {{.Name}}.service-{{.Address}}-{{.Port}} {{.Address}}:{{.Port}}
    {{end}}{{end}}{{end}}{{end}}
EOF

# Fire up the nomad job
cat <<EOF > /tmp/www-demo.nomad
job "www-demo" {
  # The "datacenters" parameter specifies the list of datacenters which should
  # be considered when placing this task. This must be provided.
  datacenters = ["dc1"]
  type = "service"

  group "load-balancer" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    task "haproxy" {
      driver = "docker"
      config {
        image = "cavemandaveman/haproxy-consul-template:latest"
        volumes = [
          "/tmp/haproxy-consul-template/:/consul-template/"
        ]
        logging {
          type = "json-file"
        }
      }

      resources {
        cpu     = 250
        memory  = 128
        network {
          port "http" {
            static = 80
          }
        }
      }
    }
  }

  group "backend" {
    # The "count" parameter specifies the number of the task groups that should
    # be running under this group. This value must be non-negative and defaults
    # to 1.
    count = 5

    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }

    task "web" {
      driver = "docker"
      config {
        image = "kencochrane/aws-demo:latest"
        port_map = {
          http = 80
        }
        logging {
          type = "json-file"
        }
      }

      resources {
        cpu    = 250
        memory = 128
        network {
          port "http" {}
        }
      }

      logs {
        max_files     = 1
        max_file_size = 5
      }

      service {
        tags = ["routed"]
        port = "http"
        check {
          type     = "http"
          port     = "http"
          path     = "/"
          interval = "10s"
          timeout  = "3s"
        }
      }
    }
  }
}
EOF

# Install nomad
cd /tmp
git clone --branch v0.0.4 https://github.com/hashicorp/terraform-aws-nomad.git
/tmp/terraform-aws-nomad/modules/install-nomad/install-nomad --version "$${NOMAD_VERSION}"

# Start nomad in dev mode and add the job
nomad agent -dev >/tmp/nomad.log 2>&1 &

# This is to keep trying until Nomad comes up...
while :; do
  nomad run /tmp/www-demo.nomad
  [ $? -eq 0 ] && break
  sleep 5
done

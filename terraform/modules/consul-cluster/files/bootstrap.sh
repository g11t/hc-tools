#!/bin/bash

set -x
exec > /var/log/user-data.log 2>&1

REGION="${region}"
EXPECTED_SIZE="${num_nodes}"

# Grab IP to bind consul to
IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

docker run -d --net=host \
    --name=consul \
    --restart=always \
    consul agent -server -ui \
    -bind="$${IP}" \
    -client="0.0.0.0" \
    -retry-join "provider=aws tag_key=Purpose tag_value=Consul-Node region=$${REGION}" \
    -bootstrap-expect="$${EXPECTED_SIZE}"

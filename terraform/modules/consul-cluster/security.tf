resource "aws_security_group" "consul-lb" {
  name        = "consul-lb"
  description = "Security group that allows inbound web traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.inbound_from}"]
  }

  egress {
    from_port       = 8500
    to_port         = 8500
    protocol        = "tcp"
    security_groups = ["${aws_security_group.consul-nodes.id}"]
  }

  tags {
    Name = "Consul Cluster - Load Balancer"
  }
}

resource "aws_security_group" "consul-nodes" {
  name        = "consul-nodes"
  description = "Security group that allows inbound traffic from the LB"
  vpc_id      = "${var.vpc_id}"

  tags {
    Name = "Consul Cluster - Nodes"
  }
}

# Using aws_security_group_rule here otherwise a cycle would be created between the two SGs
resource "aws_security_group_rule" "inbound-lb-traffic" {
  type                     = "ingress"
  from_port                = 8500
  to_port                  = 8500
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.consul-lb.id}"

  security_group_id = "${aws_security_group.consul-nodes.id}"
}

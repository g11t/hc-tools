#!/bin/bash

set -x
exec > /var/log/user-data.log 2>&1

REGION="${region}"

# Grab IP to bind consul to
IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

docker run -d --net=host \
    --restart=always \
    --name=consul \
    consul agent \
    -bind="$${IP}" \
    -retry-join "provider=aws tag_key=Purpose tag_value=Consul-Node region=$${REGION}" \


mkdir -p /usr/local/etc/vault/config /usr/local/etc/vault/policies
cat << EOF > /usr/local/etc/vault/config/vault.hcl
listener "tcp" {
    address = "0.0.0.0:8200"
    tls_disable = 1
}

storage "consul" {
    address = "127.0.0.1:8500"
    path    = "vault/"
}
EOF

cat << EOF > /usr/local/etc/vault/policies/postgres.hcl
path "secret/postgres/*" {
    policy = "read"
}
EOF

docker run -d --net=host \
    --restart=always \
    --name=vault \
    --cap-add=IPC_LOCK \
    -e VAULT_ADDR=http://localhost:8200/ \
    -v /usr/local/etc/vault/config:/vault/config \
    -v /usr/local/etc/vault/policies:/vault/policies \
    vault server

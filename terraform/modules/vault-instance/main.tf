terraform {
  required_version = ">= 0.10.3"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  owners = ["self"]

  filter {
    name   = "name"
    values = ["base-docker-*"]
  }
}

data "template_file" "bootstrap" {
  template = "${file("${path.module}/files/bootstrap.sh")}"

  vars {
    region = "${var.region}"
  }
}

resource "aws_instance" "main" {
  ami                  = "${data.aws_ami.ubuntu.image_id}"
  instance_type        = "${var.instance_type}"
  iam_instance_profile = "${aws_iam_instance_profile.main.id}"

  key_name  = "${var.key_name}"
  user_data = "${data.template_file.bootstrap.rendered}"
  subnet_id = "${var.subnet_id}"

  vpc_security_group_ids = [
    "${aws_security_group.vault.id}",
    "${var.extra_sgs}",
  ]

  lifecycle {
    create_before_destroy = true
  }

  tags {
    Name = "Vault Server"
  }
}

resource "aws_security_group" "vault" {
  name        = "vault"
  description = "Security group for vault"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  tags {
    Name = "Vault"
  }
}

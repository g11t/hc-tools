provider "aws" {
  version    = "1.3.1"
  region     = "${var.region}"
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
}

variable "aws_access_key_id" {
  description = "AWS access key ID to use"
  default     = ""
}

variable "aws_secret_access_key" {
  description = "AWS secret access key to use"
  default     = ""
}

variable "vpc_cidr" {
  default = "192.168.0.0/16"
}

variable "inbound_from" {
  description = "Limit inbound access to this range"
  default     = "0.0.0.0/0"
}

variable "public_key_path" {
  description = "Path to the public key used for SSH"
}

variable "availability_zones" {
  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1e",
  ]

  type = "list"
}

variable "region" {
  default = "us-east-1"
}

terraform {
  required_version = ">= 0.10.3"
}

provider "template" {
  version = "~> 1.0"
}
